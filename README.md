# OSPP 2021 项目： 完善 Gentoo ROCm GPGPU 框架

## 申请书
### 项目详细方案
ROCm在快速开发中，现有的许多软件包需要更新，项目首先测试所有的软件包并将其更新到最新，同时保留需要向后兼容的版本。完成更新后，对软件包进行质量检查，然后着手更新 pytorch 支持、tensorflow 支持，添加支持rocm的cupy 软件包、jax 软件包、numba 软件包等一系列可从cuda迁移的gpu加速软件框架。
主要工作是 ebuild 脚本的撰写、软件编译以及 debug、软件包质量检查。

### 项目开发时间计划
1. 2021.07.01-2021.07.20：将rocm 软件包更新。完成 sci-libs/rocBLAS、sci-libs/miopen 的质量检查，使之符合 Gentoo 的规范。
2. 2021.07.20-2021.08.15：更新 Pytorch 支持并进行质量检查；制作支持 rocm的TensorFlow 软件包，并进行质量检查。
3. 2021.08.15-2021.08.30：撰写 cupy 软件包并实现 cuda 支持；了解 jax、numba 的rocm支持现状。
4. 2021.09.01-2021.09.30：完成 cupy、jax、numba 的 rocm 支持。

## 当前进展

已完成的工作有：

1. sys-devel/llvm-roc-4.3.0 的 bug 修复，已合并至 ::gentoo
2. dev-util/hip-4.3.0 的 ebuild 清理，已合并至 ::gentoo
3. dev-util/hip-4.2.0 与 dev-util/roctracer-4.2.0 版本升级，已合并至 ::gentoo
4. sci-libs/rocBLAS-4.3.0，版本升级并添加了诸多特性：将 Tensile bundle 到构建过程中，增加 test 与 benchmark USE flag，针对 GPU 架构筛选 ASM solution。修复 glibc-2.32 带来的编译问题。已合并至 ::gentoo，支持多种 arch，可以 test benchmark。
5. sci-libs/miopen-4.3，版本升级并修复了 >boost-1.72 带来的编译问题、gcc-11 带来的编译问题。
6. 发现 cmake-3.21 支持 HIP 编程语言后无法正常 configure 已有项目的 bug，并找到了解决方案。
7. 深入理解了 TensileCreateLibrary 的机制。
8. 发现了 linux kernel <5.13 的版本在 Vega 7nm （Vega20, gfx906）架构上与 >rocm-4.0 runtime 不兼容。
9. 完成了 4.3 版本的 hipCUB, hipFFT, hipSPARSE, miopen, rocBLAS, rocFFT, rocPRIM  rocRAND, rocSPARSE, rocThrust, 并在 rocm-4.3 的环境下成功编译运行 pytorch-1.9.0。这些 ebuild 当前都可以在本仓库看到
10. cmake-3.21 适配
11. 6700XT (navi22) 初始支持。

## 仓库说明
由于项目是撰写 ebuild 与相关 patch 提交至上游，因此项目自身没有独立的仓库，所有工作通过 fork 上游仓库进行开发、向上游提交 pull request 进行。

本仓库用于暂存还未被 merge 的 ebuild 初稿，目的是在远端备份这些 ebuild，以及不让本项目的 OSPP 官方 gitlab 仓库空空如也。

## 相关链接
ebuild 最终被提交到：Gentoo 官方 https://gitweb.gentoo.org/repo/gentoo.git, Gentoo Science Overlay https://gitweb.gentoo.org/proj/sci.git.

已经向上游提交的 pull request: 
1. [rocBLAS-4.1.0](https://github.com/gentoo/sci/pull/1102)
2. [rocr-runtime-4.2.0](https://github.com/gentoo/gentoo/pull/21590)
3. [rocclr-4.2.0](https://github.com/gentoo/gentoo/pull/21654)
4. [hip-4.1.0](https://github.com/gentoo/gentoo/pull/21863)
5. [hip-4.2.0](https://github.com/gentoo/gentoo/pull/21655)
6. [roctracer-4.2.0](https://github.com/gentoo/gentoo/pull/21973)

此外，gentoo 社区中还有一部分人建立了一个 rocm ebuild 仓库：https://github.com/justxi/rocm/，用于存放开发中的 ebuilds 以及研讨开发相关事宜。项目人参与的有关讨论有：

1. [Discussions on rocBLAS and Tensile](https://github.com/justxi/rocm/issues/194)
2. [Clean up pathes in hip ebuild](https://github.com/justxi/rocm/issues/192)
3. [Current status of PyTorch build against ROCm](https://github.com/justxi/rocm/issues/133)
4. [PRs to Gentoo](https://github.com/justxi/rocm/issues/133)
5. [ROCm 4.2: building llvm/clang - undefined reference to llvm::object::getELFSectionTypeName](https://github.com/justxi/rocm/issues/190)
6. [rocBLAS from science overlay](https://github.com/justxi/rocm/issues/187)